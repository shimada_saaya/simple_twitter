package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String startedDate, String endedDate) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            Integer id = null;
            if(!StringUtils.isBlank(userId)) {
            	id = Integer.parseInt(userId);
            }
            String started_date = null;
            if(!StringUtils.isBlank(startedDate)) {
            	started_date = startedDate + " 00:00:00";
            } else {
            	started_date = "2020/01/01 00:00:00";
            }
            String ended_date = null;
            if(!StringUtils.isBlank(endedDate)) {
            	ended_date = endedDate + " 23:59:59";
            } else {
            	Date end = new Date();
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            	TimeZone timeZoneJP = TimeZone.getTimeZone("Asia/Tpkyo");
            	sdf.setTimeZone(timeZoneJP);
            	ended_date = (sdf.format(end));
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, started_date, ended_date, id, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(int id) {

    	Connection connection = null;
    	try {
    		connection = getConnection();
    		Message message = new MessageDao().select(connection, id);
    		commit(connection);

    		return message;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}

    }

    public void update(Message message) {

    	Connection connection = null;
    	try {
    		connection = getConnection();
    		new MessageDao().update(connection,  message);
    		commit(connection);
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}

    }

}